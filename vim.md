## vim常用快捷键

```txt
1. shift+g 到最后一行
2. gg 到第一行
3. vim a.xxx +x 打开 a.xxx 到第x行
4. dd 删除一行
5. uu 撤销上次的操作
6. set number 显示行数
7. set nonumber 关闭显示行数
8. 插入一列操作
```

