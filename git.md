# Git

## Git 核心概念

### HEAD

HEAD 表示头指针指向的版本，即当前版本。

HEAD^ 表示当前版本的上一个版本。

HEAD^^ 表示当前版本的上两个版本。

HEAD~X 表示当前版本的上 X 个版本。

### 工作区

git init 的时候的文件夹就是工作区。

### 版本库

工作目录下存在一个隐藏目录 `.git` ，是 Git 的版本库。

### 暂存区

Git 版本库中存放了很多东西，其中重要的称为 `Stage/Index` ，即暂存区。

还有 Git 为我们创建的默认分支 `master` ，以及指向 `master` 的指针 `HEAD` 。

## 时光穿梭机

### 创建一个Git仓库

```python
1. git init # 初始化一个Git仓库
```

### 将修改添加到 Git 版本库

```python
首先假设在工作区创建了一个文本 a.txt
1. git add a.txt # 将 a.txt 提交到暂存区
2. git commit -m "add a.txt" # 将暂存区的提交到当前分支上，即加入版本区。 -m 表示增加信息
```

### 版本回退

```python
假设已经进行了三次提交 commit，即 git 有三个版本号 
1. git reset --hard commit_id # HEAD指针指向 commit_id，即回退回 commit_id 版本。
2. git reflog # 记录了 HEAD 指针的所有指向的 commit_id 记录，即通过这可以返回任意一个 HEAD 指针指向的任意 commit_id。
```

### 管理修改

git 记录的是修改，而非文件。很好理解，就是 a.txt 文件加入到版本中，后又修改了，但却不在git版本中。

```python
1. git diff HEAD -- a.txt # 查看 Git 当前版本 和 工作区中的 diff
2. 可以多次add，后一次commit，因为add只是提交到了暂存区。
```

### 撤销修改

```python
case1: 没有 add 到暂存区。
solution: git checkout -- a.txt # git checkout 本质上是用版本库去替换工作区中的

case2: add 到暂存区，但没有 commit 到版本库。 # git reset 不仅可以回退版本，还可以把暂存区的的修改回退到工作区
solution: git reset HEAD a.txt

case3: 已经 commit 到版本区。(没有提交到远程库)
solution: 版本回退，git reset --hard commit_id # git reset 不仅可以回退版本，还可以把暂存区的的修改回退到工作区
```

### 删除文件

```python
git rm a.cpp # 此时版本库和暂缓区都没有 a.cpp 了，若是这种方式删错只能回退版本
rm -rf a.cpp # 此种方式删除，可以使用 git checkout -- a.txt，用版本区替换工作区。
```

## 分支管理

### 创建并切换分支

```python
1. git checkout -b dev # 创建分支 dev 并切换到 dev 上

等价于:
1. git branch dev
2. git checkout dev
```

### 合并分支

```python
# 在 dev 分支开发完成，add & commit 后，切换回 master 分支，把 dev分支合并到 master 上
git merge dev # 把指定分支合并到当前分支
```

### 删除分支

```python
1. git branch -d dev # 删除 dev分支，此时dev已经合并到其他分支。
2. git branch -D dev # 删除 dev分支，此时dev没有合并到其他分支。 
```

### 解决分支冲突

```python
两个不同分支分别对同一文件修改 并且 提交了commit，合并到一起需要先解决冲突，在 add & commit

git log --graph # 查看分支合并图
```

### Bug 分支

```python
当突然需要解决bug，且此时你在开发 feature 分支上有修改，但还不能提交时，此时需要切换到另一个分支，但当前分支修改必须提交或者贮藏(stash)。
1. git stash # 贮藏当前的修改
# 贮藏后就可以切换到其他分支了
2. git stash pop # 切换回 feature分支，取出装stash。注意可以多次 stash 并且选择拿出哪些。
3. git stash list # 查看当前有哪些 stash 
```

### 多人协作

```python
1. git branch --set-upstream-to=origin/dev dev # 本地dev分支和远程dev分支建立链接
2. git checkout -b dev origin/dev # 建立dev分支，并且和远程dev分支建立链接。因为默认是从master分支链接的。
3. git push前 要先 git pull
```